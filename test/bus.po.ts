import { browser, by, element } from 'protractor';

export class BusPage {
  navigateTo() {
    return browser.get('/bus') as Promise<any>;
  }

  getTypeaheadInput() {
    return element(by.css('#typeahead-basic'));
  }

  getFirstTypeAheadSuggestion() {
    return element(by.css('#ngb-typeahead-0-0'));
  }
}
