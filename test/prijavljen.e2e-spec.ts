import { HomePage } from './home.po';
import { browser, logging, ExpectedConditions, element, by } from 'protractor';
import { AppPage } from './app.po';
import { PrijavaPage } from './prijava.po';

describe('workspace-project App', () => {
  let appPage: AppPage;
  let homePage: HomePage;
  let prijavaPage: PrijavaPage;

  beforeEach(() => {
    appPage = new AppPage();
    homePage = new HomePage();
    prijavaPage = new PrijavaPage();

    browser.driver.manage().window().maximize();
    appPage.navigateTo();
    browser.waitForAngularEnabled(true);

    appPage.getLoginDropdown().click();
    appPage.getLoginButton().click();

    prijavaPage.getEmailInput().sendKeys('uporabnik@uporabnik.com');
    prijavaPage.getPassword1Input().sendKeys('uporabnik');
    prijavaPage.getSubmitButton().click();

    browser.waitForAngularEnabled(false);
    browser.wait(ExpectedConditions.textToBePresentInElement(appPage.getLoginDropdown(), 'uporabnik@uporabnik.com'),
      5000, 'Element taking too long to appear in the DOM');
  });

  it('TODO - dodajanje', () => {
    browser.wait(ExpectedConditions.presenceOf(homePage.getTodoAddButton()), 5000, 'Element taking too long to appear in the DOM');
    homePage.getTodoAddButton().click();
    browser.wait(ExpectedConditions.presenceOf(homePage.getTodoAddinput()), 5000, 'Element taking too long to appear in the DOM');
    homePage.getTodoAddinput().sendKeys('TODO vnos 123');
    homePage.getTodoAddConfirm().click();
    browser.sleep(1000);
    expect(homePage.getTodoLastItem().getText()).toEqual('TODO vnos 123');
  });

  afterEach(() => {
    browser.restart();
  });
});
