import { browser, by, element } from 'protractor';

export class FoodPage {
  navigateTo() {
    return browser.get('/food') as Promise<any>;
  }
}
