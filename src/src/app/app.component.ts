import { Arrival } from './modeli/Arrival';
import { Component } from '@angular/core';
import { Koledar } from './modeli/Koledar';
import { Urnik, Dan } from './modeli/Urnik';
import { Todo } from './modeli/Todo';
import { Postaja } from './modeli/Postaja';
import { AdministratorskaObvestila } from './modeli/AdministratorskaObvestila';
import { KrmilnikZaAvtentikacijoUporabnika } from './krmilniki/KrmilnikZaAvtentikacijoUporabnika';
import { KrmilnikZaObvestila } from './krmilniki/KrmilnikZaObvestila';
import { KrmilnikZaDogodke } from './krmilniki/KrmilnikZaDogodke';
import { KrmilnikZaAvtobuse } from './krmilniki/KrmilnikZaAvtobuse';
import { KrmilnikZaRestavracije } from './krmilniki/KrmilnikZaRestavracije';
import { KrmilnikZaKoledar } from './krmilniki/KrmilnikZaKoledar';
import { KrmilnikZaUrnik } from './krmilniki/KrmilnikZaUrnik';
import { KrmilnikZaTodo } from './krmilniki/KrmilnikZaTodo';
import { Observable } from 'rxjs';
import { UporabnikService } from './modeli/UporabnikService';
import { RouterModule, Routes } from '@angular/router';
import { AngularFirestore, AngularFirestoreCollection } from '@angular/fire/firestore';

import { map, first } from 'rxjs/operators';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {

  title = 'StraightAs';
  koledarVnosi: Observable<Koledar[]>;
  urnikVnosi: Observable<Urnik[]>;
  todoVnosi: Observable<Todo[]>;
  obvestila$: Observable<AdministratorskaObvestila[]>;

  constructor(
    public krmilnikZaAvtentikacijoUporabnika: KrmilnikZaAvtentikacijoUporabnika,
    public krmilnikZaObvestila: KrmilnikZaObvestila,
    public krmilnikZaDogodke: KrmilnikZaDogodke,
    public krmilnikZaAvtobuse: KrmilnikZaAvtobuse,
    public krmilnikZaRestavracije: KrmilnikZaRestavracije,
    public krmilnikZaKoledar: KrmilnikZaKoledar,
    public krmilnikZaUrnik: KrmilnikZaUrnik,
    public krmilnikZaTodo: KrmilnikZaTodo,
    public uporabnikService: UporabnikService,
    public krmilnikAvtobus: KrmilnikZaAvtobuse,
    public readonly afs: AngularFirestore,
    public obvestilaService: KrmilnikZaObvestila
  ) {

    /*
    this.koledarVnosi = this.krmilnikZaKoledar.vrni_vnose_iz_koledarja('pepi@gmail.com');
    this.urnikVnosi = this.krmilnikZaUrnik.vrni_vnose_iz_urnika('pepi@gmail.com');
    this.todoVnosi = this.krmilnikZaTodo.vrni_vnose_iz_todo('pepi@gmail.com');
    this.obvestila = this.krmilnikZaObvestila.vrni_obvestila();

    this.krmilnikZaKoledar.dodaj_vnos_v_koledar('koledarski vnos', 'pepi@gmail.com', new Date('2019-12-17T03:24:00'), 'Čas vnosa: '+new Date().toString(), 5);
    this.krmilnikZaUrnik.dodaj_vnos_v_urnik('pepi@gmail.com', 'vnos v urniku', 2, '#AA5555', Dan.ponedeljek, 8);
    this.krmilnikZaTodo.dodaj_vnos_v_todo('pepi@gmail.com', 'Čas vnosa: '+new Date().toString());
    this.krmilnikZaObvestila.dodaj_obvestilo('Pomembno obvestilo! ('+new Date().toString()+')');*/

    /*this.koledarService.dodaj_vnos_v_koledarju(
       'Banana', 'pepi@gmail.com', new Date('2019-12-17T03:24:00'), 'Delaaaaa', 5).then(() => {
         console.log("Uspešen vnos!");
         this.koledarService.vrni_vse_vnose('pepi@gmail.com').subscribe({
           next( k: Koledar[] ) {
             console.log(k);
           }
         });
       });*/
    /* this.urnikService.dodaj_vnos_v_urnik(
    'pepi@gmail.com', 'lol', 2, "rdeca", Dan.ponedeljek, 8).then(() => {
      console.log("Uspešen vnos urnika!");
      this.urnikService.vrni_vse_vnose('pepi@gmail.com').subscribe({
        next( k: Urnik[] ) {
          console.log(k);
        }
      });
    });

    this.todoService.ustvari_vnos('pepi@gmail.com', 'test').then(() => {
      console.log('Uspešen vnos!');
      this.todoService.posodobi_vnos('pepi@gmail.com', 'updated', 'sFR96iRuLsC4JMrd3Scp').then(() => {
        console.log('vnos posodobljen');
        this.todoService.vrni_vnose('pepi@gmail.com').subscribe({
          next( todos: Todo[]) {
            console.log(todos);
          }
        });
      });
    });

    this.obvestiloService.dodaj_obvestilo('Admin je boss!;)').then(() => {
      this.obvestiloService.vrni_obvestila().subscribe({
        next(obvestila: AdministratorskaObvestila[]) {
          console.log(obvestila);
        }
      });
    });  */
    /*const urnikS = this.urnikService;
    this.urnikService.vrni_vse_vnose('pepi@gmail.com').subscribe({
      next( k: Urnik[] ) {
        this.unsubscribe();
        console.log(k);
        let x: Urnik = k[0];
        urnikS.izbrisi_vnos_iz_urnika(x).then(() => {
          console.log('izbrisano');
        });
        let y: Urnik = k[1];
        y.ime = 'Trol';
        urnikS.posodobi_vnos_v_urniku(y).then(() => {
          console.log('posodobljeno');
        });
      }
    });
    this.urnikService.vrni_vse_vnose('pepi@gmail.com').subscribe({
      next( k: Urnik[] ) {
        this.unsubscribe();
        console.log(k);
      }
    });*/

    /* 
    krmilnikAvtobus.stations.subscribe(array => console.log(array));
    krmilnikAvtobus.routes.subscribe(array => console.log(array));

    krmilnikAvtobus.vrni_podatke_o_prihodih(1858).subscribe(array => console.log(array));
    krmilnikAvtobus.getStation(1858).subscribe(array => console.log(array));
     */
  }

  ngOnInit() {
    this.obvestila$ = this.obvestilaService.vrni_obvestila();
  }
}
