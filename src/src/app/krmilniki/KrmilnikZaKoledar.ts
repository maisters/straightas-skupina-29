import { Injectable } from '@angular/core';
import { Koledar } from '../modeli/Koledar';
import { AngularFirestore, AngularFirestoreCollection } from '@angular/fire/firestore';
import { Observable } from 'rxjs';
import { firestore } from 'firebase';

@Injectable({
  providedIn: 'root',
})
export class KrmilnikZaKoledar {

  private koledarCollection: AngularFirestoreCollection<Koledar>;

  constructor(private readonly afs: AngularFirestore) {
    this.koledarCollection = afs.collection<Koledar>('koledar');
  }

  dodaj_vnos_v_koledar(ime: string, email: string, zacetniDatumDate: Date, koncniDatumDate: Date, barva: string, opis: string): Promise<void> {
    const id = this.afs.createId();
    if (!this.preveri_veljavnost_datuma(zacetniDatumDate) && !this.preveri_veljavnost_datuma(koncniDatumDate)) {
      return null;
    }
    let zacetniDatum = firestore.Timestamp.fromDate(zacetniDatumDate);
    let koncniDatum = firestore.Timestamp.fromDate(koncniDatumDate);
    const koledar: Koledar = { id, ime, email, zacetniDatum, koncniDatum, barva, opis };
    return this.koledarCollection.doc(id).set(koledar);
  }

  posodobi_vnos_v_koledarju(koledar: Koledar): Promise<void> {
    if (!this.preveri_veljavnost_datuma(koledar.zacetniDatum.toDate()) && !this.preveri_veljavnost_datuma(koledar.koncniDatum.toDate())) {
      return null;
    }
    return this.koledarCollection.doc(koledar.id).update(koledar);
  }

  izbrisi_vnos_iz_koledarja(koledar: Koledar): Promise<void> {
    return this.koledarCollection.doc(koledar.id).delete();
  }

  vrni_vnose_iz_koledarja(email: string): Observable<Koledar[]> {
    return this.afs.collection<Koledar>('koledar', ref => ref.where('email', '==', email)).valueChanges();
  }

  preveri_veljavnost_datuma(datum: Date): boolean {
    return datum > new Date();
  }

}
