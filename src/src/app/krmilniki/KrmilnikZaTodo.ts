import { Injectable } from '@angular/core';
import { Todo } from '../modeli/Todo';
import { AngularFirestore, AngularFirestoreCollection } from '@angular/fire/firestore';
import { Observable } from 'rxjs';
import { firestore } from 'firebase';

@Injectable({
  providedIn: 'root',
})
export class KrmilnikZaTodo {

  private TodoCollection: AngularFirestoreCollection<Todo>;
  

  constructor(private readonly afs: AngularFirestore) {
    this.TodoCollection = afs.collection<Todo>('todo');
  }

  dodaj_vnos_v_todo(email: string, opis: string): Promise<void> {
    const id = this.afs.createId();
    const datum = firestore.Timestamp.fromDate(new Date());
    const todo: Todo = { id, email, datum, opis: '' };
    return this.TodoCollection.doc(id).set(todo);
  }

  posodobi_vnos_v_todo(email: string, opis: string, id: string): Promise<void> {
    const datum = firestore.Timestamp.fromDate(new Date());
    const todo: Todo = { id, email, datum, opis };
    return this.TodoCollection.doc(id).update(todo);
  }

  izbrisi_vnos_iz_todo(id: string): Promise<void> {
    return this.TodoCollection.doc(id).delete();
  }

  vrni_vnose_iz_todo(email: string): Observable<Todo[]> {
    return this.afs.collection<Todo>('todo', ref => ref.where('email', '==', email)).valueChanges();
  }

}
