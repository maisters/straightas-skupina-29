import { Injectable } from '@angular/core';
import { Restavracija } from '../modeli/Restavracija';

import * as restavracije from '../ostalo/restavracija.json';

import { AngularFirestore, AngularFirestoreCollection } from '@angular/fire/firestore';
import { Observable } from 'rxjs';
import { firestore } from 'firebase';

import { QuerySnapshot } from '@angular/fire/firestore';

@Injectable({
  providedIn: 'root',
})
export class KrmilnikZaRestavracije {

  private RestavracijaCollection: AngularFirestoreCollection<Restavracija>;



  constructor(private readonly afs: AngularFirestore) { }

  vrni_podatke_o_restavracijah(lat:number, lng: number) {

    let obj:any = restavracije.restavracija;

    obj.forEach((element) => element.oddaljenost = this.calculateDistance(element.lat, lat, element.lng, lng));

    obj.sort((a, b)=> (a.oddaljenost > b.oddaljenost) ? 1 : -1);

    return obj;
  }

  calculateDistance(lat1:number,lat2:number,long1:number,long2:number){
    let p = 0.017453292519943295;    // Math.PI / 180
    let c = Math.cos;
    let a = 0.5 - c((lat1-lat2) * p) / 2 + c(lat2 * p) *c((lat1) * p) * (1 - c(((long1- long2) * p))) / 2;
    let dis = (12742 * Math.asin(Math.sqrt(a))); // 2 * R; R = 6371 km
    return dis;
  }

  /* vrni_vnose_iz_todo(email: string): Observable<Todo[]> {
    return this.afs.collection<Todo>('todo', ref => ref.where('email', '==', email)).valueChanges();
  } */
}
