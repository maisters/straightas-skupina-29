import { Injectable, NgZone } from '@angular/core';
import { UporabnikService } from '../modeli/UporabnikService';
import { Uporabnik } from '../modeli/Uporabnik';

import { AngularFireAuth } from '@angular/fire/auth';
import {
  AngularFirestore,
  AngularFirestoreDocument
} from '@angular/fire/firestore';

import { Router } from '@angular/router';
import * as firebase from 'firebase';
import {
  CheckboxControlValueAccessor,
  SelectControlValueAccessor
} from '@angular/forms';

@Injectable({
  providedIn: 'root'
})
export class KrmilnikZaAvtentikacijoUporabnika {
  /* VARS */

  uspeh: string;
  napaka: string;

  /* CONSTRUCTOR */

  constructor(
    public afs: AngularFirestore, // Inject Firestore service
    public afAuth: AngularFireAuth, // Inject Firebase auth service
    public router: Router,
    public ngZone: NgZone, // NgZone service to remove outside scope warning
    public uporabnikService: UporabnikService
  ) {}

  /* MAIN METHODS */

  /**
   * @description modelu posreduje ukaze za registracijo in vrne statusno kodo glede na uspeh postopka
   *
   * @param email String
   * @param geslo1 String
   * @param geslo2 String
   */
  izvedi_registracijo(email, geslo1, geslo2) {
    if (!this.uporabnikService.preveri_veljavnost_gesla(geslo1, geslo2)) {
      this.napaka = 'Gesli se ne ujemata';
      this.uspeh = '';
    } else {
      this.afAuth.auth
        .createUserWithEmailAndPassword(String(email.trim()), geslo1)
        .then(result => {
          this.uspeh = 'Registracija uspešna';
          this.router.navigate(['']);
          this.uporabnikService.poslji_potrditveni_email();
          this.napaka = '';
          this.uspeh = '';
        })
        .catch(error => {
          this.uspeh = '';
          switch (error.code) {
            case 'auth/email-already-in-use':
              this.napaka = 'Email naslov je že v uporabi';
              break;
            case 'auth/invalid-email':
              this.napaka = 'Nepravilen naslov';
              break;
            case 'auth/operation-not-allowed':
              this.napaka = 'Neveljaven zahtevek';
              break;
            case 'auth/weak-password':
              this.napaka = 'Izberi močnejše geslo.';
              break;
            default:
              this.napaka = 'Nekaj je šlo narobe. Poskusi šeenkrat.';
          }
        });
    }
  }

  /**
   * @description Modelu posreduje ukaze za prijavo in vrne statusno kodo glede na uspeh postopka.
   *
   * @param email String
   * @param geslo String
   */
  izvedi_prijavo(email, geslo) {
    return this.afAuth.auth
      .signInWithEmailAndPassword(email, geslo)
      .then(result => {
        this.uspeh = 'Uspešna prijava';
        this.napaka = '';
        this.router.navigate(['/']);
        this.uspeh = '';
        this.uporabnikService.uporabnik = result.user;
      })
      .catch(error => {
        this.uspeh = '';
        switch (error.code) {
          case 'auth/invalid-email':
            this.napaka = 'Neveljavni email naslov.';
            break;
          case 'auth/user-disabled':
            this.napaka = 'Vaš račun je blokrian';
            break;
          case 'auth/user-not-found':
            this.napaka = 'Uporabnik s tem email naslovom ni najdem.';
            break;
          case 'auth/wrong-password':
            this.napaka = 'Napačeno geslo.';
            break;
          default:
            this.napaka = 'Neki ne dela. Verjetno vaša napaka.';
        }
      });
  }

  /**
   * @description Modelu posreduje ukaze za spremembo gesla in vrne statusno kodo glede na uspeh postopka.
   *
   * @param novo_geslo String
   */
  spremeni_geslo(geslo, novo_geslo1, novo_geslo2) {
    if (novo_geslo1 !== novo_geslo2) {
      this.napaka = 'Gesli se ne ujemata';
      return false;
    } else {
      const user1 = firebase.auth().currentUser;
      const credential = firebase.auth.EmailAuthProvider.credential(
        user1.email,
        geslo
      );

      this.afAuth.auth.currentUser
        .reauthenticateWithCredential(credential)
        .then(() => {
          this.afAuth.auth.currentUser
            .updatePassword(novo_geslo1)
            .then(() => {
              this.uspeh = 'Sprememba gesla uspela';
              this.router.navigate(['/']);
              this.napaka = '';
              this.uspeh = '';
            })
            .catch(error => {
              this.napaka = 'Sprememba ni uspela';
              this.uspeh = '';
            });
        })
        .catch(error => {
          this.napaka = 'Staro geslo ni pravilo';
          this.uspeh = '';
        });
    }
  }

  /**
   * @description Modelu posreduje ukaze za avtentikacijo seje in vrne statusno kodo glede na uspeh postopka.
   *
   * OPOMBA: uporabnikov_email in dostopni_zeton izpuscena, podatke se pridobiva iz trenutno prijavljenega
   * auth.currentUser
   */
  avtenticiraj_sejo() {
    const user = this.uporabnikService.afAuth.auth.currentUser;
    return user !== null ? true : false;
  }

  /**
   * @description Preveri če je mail trenutnega uporabnika potrjen
   */
  preveri_mail() {
    const user = this.uporabnikService.afAuth.auth.currentUser;
    if (user == null) {
      return true;
    }
    return user !== null && user.emailVerified !== false ? true : false;
  }

  /**
   * @description Metoda ni opredeljena v načrtu, a je nepogrešljiva pri take aplikaciji
   * ~nezadovoljen naročnik, ki sedaj sam kodira aplikacijo
   */
  odjava() {
    this.afAuth.auth.signOut().then(() => {
      this.router
        .navigateByUrl('/prijava', { skipLocationChange: true })
        .then(() => this.router.navigate(['/']));
    });
  }

  /** Helper methods */
}
