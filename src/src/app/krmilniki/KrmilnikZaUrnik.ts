import { Injectable } from '@angular/core';
import { Urnik, Dan } from '../modeli/Urnik';
import { AngularFirestore, AngularFirestoreCollection } from '@angular/fire/firestore';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root',
})
export class KrmilnikZaUrnik {

  private urnikCollection: AngularFirestoreCollection<Urnik>;

  constructor(private readonly afs: AngularFirestore) {
    this.urnikCollection = afs.collection<Urnik>('urnik');
  }

  dodaj_vnos_v_urnik(email: string, ime: string, trajanje: number, barva: string, dan: Dan, ura: number): Promise<void> {
    const id = this.afs.createId();
    const urnik: Urnik = { id, email, ime, barva, trajanje, dan, ura };
    return this.urnikCollection.doc(id).set(urnik);
  }

  posodobi_vnos_v_urniku(urnik: Urnik): Promise<void> {
    return this.urnikCollection.doc(urnik.id).update(urnik);
  }

  izbrisi_vnos_iz_urnika(urnik: Urnik): Promise<void> {
    return this.urnikCollection.doc(urnik.id).delete();
  }

  vrni_vnose_iz_urnika(email: string): Observable<Urnik[]> {
    return this.afs.collection<Urnik>('urnik', ref => ref.where('email', '==', email)).valueChanges();
  }

}
