import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

// Import canActivate guard services
import { AuthGuard } from './ostalo/auth.guard';
import { AdminGuard } from './ostalo/admin.guard';
import { UpravljalecGuard } from './ostalo/upravljalec.guard';
import { SecureInnerPagesGuard } from './ostalo/secure-inner-pages.guard';

// Required components for which route services to be activated
import { AdminPogled } from './pogledi/AdminPogled';
import { BusPogled } from './pogledi/BusPogled';
import { EventsPogled } from './pogledi/EventsPogled';
import { FoodPogled } from './pogledi/FoodPogled';
import { HomePogled } from './pogledi/HomePogled';
import { PrijavniObrazec } from './pogledi/PrijavniObrazec';
import { RegistracijskiObrazec } from './pogledi/RegistracijskiObrazec';
import { SpremembaGeslaObrazec } from './pogledi/SpremembaGeslaObrazec';
import { UpravljalcevPogled } from './pogledi/UpravljalcevPogled';



const routes: Routes = [
  { path: 'admin', component: AdminPogled, canActivate: [AdminGuard]},
  { path: 'bus', component: BusPogled, canActivate: []},
  { path: 'event', component: EventsPogled, canActivate: [AuthGuard]},
  { path: 'food', component: FoodPogled, canActivate: []},
  { path: '', component: HomePogled, canActivate: []},
  { path: 'prijava', component: PrijavniObrazec, canActivate: [SecureInnerPagesGuard]},
  { path: 'registracija', component: RegistracijskiObrazec, canActivate: [SecureInnerPagesGuard]},
  { path: 'sprememba-gesla', component: SpremembaGeslaObrazec, canActivate: [AuthGuard]},
  { path: 'upravljalec', component: UpravljalcevPogled, canActivate: [UpravljalecGuard]},
  { path: '**', component: HomePogled }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
