import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';
import localeSl from '@angular/common/locales/sl';

import { AngularFireModule } from '@angular/fire';
import { AngularFirestoreModule } from '@angular/fire/firestore';
import { AngularFireAuthModule } from '@angular/fire/auth';
import { environment } from '../environments/environment';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { CommonModule, registerLocaleData } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { FlatpickrModule } from 'angularx-flatpickr';

import { CalendarModule, DateAdapter, CalendarNativeDateFormatter, DateFormatterParams, CalendarDateFormatter } from 'angular-calendar';
import { adapterFactory } from 'angular-calendar/date-adapters/date-fns';

import { UpravljalcevPogled } from './pogledi/UpravljalcevPogled';
import { EventsPogled } from './pogledi/EventsPogled';
import { AdminPogled } from './pogledi/AdminPogled';
import { RegistracijskiObrazec } from './pogledi/RegistracijskiObrazec';
import { PrijavniObrazec } from './pogledi/PrijavniObrazec';
import { SpremembaGeslaObrazec } from './pogledi/SpremembaGeslaObrazec';
import { HomePogled } from './pogledi/HomePogled';
import { FoodPogled } from './pogledi/FoodPogled';
import { BusPogled } from './pogledi/BusPogled';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

import { AgmCoreModule } from '@agm/core';

// Boostrap
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { NgbModalModule } from '@ng-bootstrap/ng-bootstrap';

import { MDBBootstrapModule } from 'angular-bootstrap-md';

import { ReactiveFormsModule } from '@angular/forms';

registerLocaleData(localeSl);

export class CustomDateFormatter extends CalendarNativeDateFormatter {
  public weekViewHour({date, locale}: DateFormatterParams): string {
    return new Intl.DateTimeFormat('sl', {
      hour: 'numeric',
      minute: 'numeric'
    }).format(date);
  }
}

@NgModule({
  declarations: [
    AppComponent,
    UpravljalcevPogled,
    EventsPogled,
    AdminPogled,
    RegistracijskiObrazec,
    PrijavniObrazec,
    SpremembaGeslaObrazec,
    HomePogled,
    FoodPogled,
    BusPogled
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    AppRoutingModule,
    AngularFireModule.initializeApp(environment.firebaseConfig),
    AngularFirestoreModule,
    AngularFireAuthModule,
    NgbModule,
    ReactiveFormsModule,
    CommonModule,
    FormsModule,
    NgbModalModule,
    BrowserAnimationsModule,
    FlatpickrModule.forRoot(),
    CalendarModule.forRoot({
      provide: DateAdapter,
      useFactory: adapterFactory
    }),
    MDBBootstrapModule.forRoot(),
    AgmCoreModule.forRoot({
      // please get your own API key here:
      // https://developers.google.com/maps/documentation/javascript/get-api-key?hl=en
      apiKey: 'AIzaSyCEeGG28e0EzbZTkrnYzr-zEP2gnbw4klE'
    })
  ],
  providers: [{
    provide: CalendarDateFormatter,
    useClass: CustomDateFormatter
  }],
  bootstrap: [AppComponent]
})
export class AppModule { }
