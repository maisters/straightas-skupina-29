import { Component, OnInit } from '@angular/core';
// Modalec
import {NgbModal, ModalDismissReasons} from '@ng-bootstrap/ng-bootstrap';
// Forma
import { FormGroup,  FormBuilder,  Validators } from '@angular/forms';
import { modalConfigDefaults } from 'angular-bootstrap-md/lib/modals/modal.options';
import { Dogodek } from '../modeli/Dogodek';
import { Observable } from 'rxjs';
import { KrmilnikZaDogodke } from '../krmilniki/KrmilnikZaDogodke';

@Component({
  selector: 'UpravljalcevPogled',
  templateUrl: './UpravljalcevPogled.html'
})
export class UpravljalcevPogled implements OnInit {

  /**
   * VARIABLES
   */

  dogodki$: Observable<Dogodek[]>;

  // Modal
  closeResult: string;
  // Forma
  dogodekObrazec: FormGroup;

  /**
   * CONSTRUCTOR
   */
  constructor(
    private modalService: NgbModal,
    private fb: FormBuilder,
    private krmilnik: KrmilnikZaDogodke
  ) {
    this.createForm();
  }

  ngOnInit() {
    this.dogodki$ = this.krmilnik.pridobi_dogodke();
  }

  /**
   * METHODS - from docs
   */

  // Done w/ Angular
  dodaj_dogodek() {
    this.modalService.dismissAll();
    this.krmilnik.objavi_dogodek(
      this.dogodekObrazec.controls.ime.value,
      this.dogodekObrazec.controls.datum.value,
      this.dogodekObrazec.controls.organizator.value,
      this.dogodekObrazec.controls.opis.value
    );
  }

  // Done w/ Angular
  objavi_dogodek() {}

  // Done w/ Angular
  preklic_dodajanja_dogodka() {}

  // Done w/ Angular
  prikazi_obvestilo_o_napaki(napaka: String) {}


  /**
   * METHODS - by me to get shit done
   */

  // Start of Modal
  open(content: any) {
    this.modalService.open(content, {ariaLabelledBy: 'modal-basic-title'}).result.then((result) => {
      this.closeResult = `Closed with: ${result}`;
    }, (reason) => {
      this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
    });
  }

  private getDismissReason(reason: any): string {
    if (reason === ModalDismissReasons.ESC) {
      return 'by pressing ESC';
    } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
      return 'by clicking on a backdrop';
    } else {
      return  `with: ${reason}`;
    }
  }
  // End of Modal
  // Start of Form
  createForm() {
    this.dogodekObrazec = this.fb.group({
       ime: ['', Validators.required ],
       datum : ['', Validators.required],
       organizator : ['', Validators.required],
       opis : ['', Validators.required]
    });
  }
  // End of form

}