import { KrmilnikZaAvtobuse } from './../krmilniki/KrmilnikZaAvtobuse';
import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { debounceTime, distinctUntilChanged, switchMap, map } from 'rxjs/operators';
import { Postaja } from '../modeli/Postaja';
import { NgbTypeaheadSelectItemEvent } from '@ng-bootstrap/ng-bootstrap';
import { LiveArrival } from '../modeli/LiveArrival';

@Component({
  selector: 'BusPogled',
  templateUrl: './BusPogled.html'
})
export class BusPogled implements OnInit {

  prihodi$: Observable<LiveArrival[]>;
  postaja: Postaja;

  constructor(private krmilnikZaAvtobuse: KrmilnikZaAvtobuse) { }

  ngOnInit() {
    this.krmilnikZaAvtobuse.stations$.subscribe().unsubscribe();
  }

  search = (text$: Observable<string>) =>
    text$.pipe(
      debounceTime(200),
      distinctUntilChanged(),
      switchMap(term =>
        this.krmilnikZaAvtobuse.stations$.pipe(
          map((postaje: Postaja[]) =>
            postaje.filter(p => this.postajaToString(p).toLowerCase().indexOf(term.toLowerCase()) > -1).slice(0, 10)
          )
        )
      )
    )

  postajaToString(postaja: Postaja): string {
    return postaja.name + ' (' + postaja.ref_id + ')';
  }

  prikazi_seznam_avtobusov(event: NgbTypeaheadSelectItemEvent) {
    const postaja: Postaja = event.item;
    this.prihodi$ = this.krmilnikZaAvtobuse.vrni_podatke_o_prihodih(postaja.int_id);
  }

}
