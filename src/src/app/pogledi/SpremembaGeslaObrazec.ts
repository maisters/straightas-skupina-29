import { Component, OnInit } from '@angular/core';

import { KrmilnikZaAvtentikacijoUporabnika } from '../krmilniki/KrmilnikZaAvtentikacijoUporabnika';

import { FormGroup,  FormBuilder,  Validators } from '@angular/forms';

@Component({
  selector: 'SpremembaGeslaObrazec',
  templateUrl: './SpremembaGeslaObrazec.html'
})
export class SpremembaGeslaObrazec implements OnInit {

  spremembaGeslaObrazec: FormGroup;

  constructor(
    public krmilnik: KrmilnikZaAvtentikacijoUporabnika,
    private fb: FormBuilder
  ) {
    this.createForm();
   }

  ngOnInit() {
  }

  createForm() {
    this.spremembaGeslaObrazec = this.fb.group({
       geslo: ['', Validators.required ],
       geslo1 : ['', Validators.required],
       geslo2 : ['', Validators.required]
    });
  }

  spremeni_geslo(){
    this.krmilnik.spremeni_geslo(
      this.spremembaGeslaObrazec.controls['geslo'].value,
      this.spremembaGeslaObrazec.controls['geslo1'].value,
      this.spremembaGeslaObrazec.controls['geslo2'].value
    )
  }

}