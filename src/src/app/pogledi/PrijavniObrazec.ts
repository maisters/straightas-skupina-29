import { Component, OnInit } from '@angular/core';
import { KrmilnikZaAvtentikacijoUporabnika } from '../krmilniki/KrmilnikZaAvtentikacijoUporabnika';

import { FormGroup,  FormBuilder,  Validators } from '@angular/forms';

@Component({
  selector: 'PrijavniObrazec',
  templateUrl: './PrijavniObrazec.html'
})

export class PrijavniObrazec implements OnInit {

  prijavniObrazec: FormGroup;

  constructor(
    public krmilnik: KrmilnikZaAvtentikacijoUporabnika,
    private fb: FormBuilder
  ) {
    this.createForm();
  }

  ngOnInit() {
  }


  createForm() {
    this.prijavniObrazec = this.fb.group({
       email: ['', Validators.required ],
       geslo : ['', Validators.required]
    });
  }


  /**
   * 
   */
  prijavi_se(){
    this.krmilnik.izvedi_prijavo(this.prijavniObrazec.controls['email'].value, this.prijavniObrazec.controls['geslo'].value);
  }

  /**
   * 
   * @param napaka 
   */
  prikazi_obvestilo_o_napaki(napaka){
    // TODO
  }
}
