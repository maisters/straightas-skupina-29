import { Component, OnInit, TemplateRef, ViewChild } from '@angular/core';

import { KrmilnikZaAvtentikacijoUporabnika } from '../krmilniki/KrmilnikZaAvtentikacijoUporabnika';

import { FormGroup,  FormBuilder,  Validators } from '@angular/forms';

import { NgbModal } from '@ng-bootstrap/ng-bootstrap';



@Component({
  selector: 'RegistracijskiObrazec',
  templateUrl: './RegistracijskiObrazec.html'
})
export class RegistracijskiObrazec implements OnInit {

  registracijskiObrazec: FormGroup;

  constructor(
    public krmilnik: KrmilnikZaAvtentikacijoUporabnika,
    private fb: FormBuilder,
    private modal: NgbModal
  ) {
    this.createForm();
  }

  ngOnInit() {

  }

  @ViewChild('modalPogoji') modalPogoji: TemplateRef<any>;

  createForm() {
    this.registracijskiObrazec = this.fb.group({
       email: ['', Validators.required ],
       geslo1 : ['', Validators.required],
       geslo2 : ['', Validators.required]
    });
  }

  registriraj_se(){
    this.krmilnik.izvedi_registracijo(
      this.registracijskiObrazec.controls['email'].value,
      this.registracijskiObrazec.controls['geslo1'].value,
      this.registracijskiObrazec.controls['geslo2'].value
    );
  }

  pogojiUporabe(){
    this.modal.open(this.modalPogoji, { size: 'lg' });
    return false;
  }

  /**
   * 
   * @param napaka
   */
  prikazi_obvestilo_o_napaki(napaka){
    // TODO
  }

}
