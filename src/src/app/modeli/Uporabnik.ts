export class Uporabnik {
    uid: string;
    email: string;
    emailVerified: boolean;
}