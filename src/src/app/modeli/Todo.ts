export class Todo {
    id: string;
    email: string;
    datum: firebase.firestore.Timestamp;
    opis: string;
}