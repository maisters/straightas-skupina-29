export class Dogodek {
    id: string;
    ime: string;
    datum: string;
    organizator: string;
    opis: string;
}
