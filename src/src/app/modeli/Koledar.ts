export class Koledar {
    id: string;
    ime: string;
    email: string;
    zacetniDatum: firebase.firestore.Timestamp;
    koncniDatum: firebase.firestore.Timestamp;
    barva: string;
    opis: string;
}
