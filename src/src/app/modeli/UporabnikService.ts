import { Injectable, NgZone } from '@angular/core';
import { Uporabnik } from './Uporabnik';
import { auth } from 'firebase/app';
import { AngularFireAuth } from '@angular/fire/auth';
import { AngularFirestore, AngularFirestoreDocument } from '@angular/fire/firestore';
import { Router } from '@angular/router';
import * as firebase from 'firebase';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root',
})
export class UporabnikService {

  uporabnik:any = null;

  uporabnikJeAdmin = false;
  uporabnikJeUpravljalec = false;

  constructor(
    public afs: AngularFirestore,   // Inject Firestore service
    public afAuth: AngularFireAuth, // Inject Firebase auth service
    public router: Router,
    public ngZone: NgZone // NgZone service to remove outside scope warning
  ) {

    this.afAuth.auth.setPersistence(firebase.auth.Auth.Persistence.LOCAL);

    this.afAuth.auth.onAuthStateChanged((user: any) => {
      localStorage.setItem('user', JSON.stringify(user));
      if (!user) {
        this.uporabnik = user;
        this.uporabnikJeAdmin = false;
        this.uporabnikJeUpravljalec = false;
      } else {
        this.afAuth.auth.currentUser.getIdTokenResult().then((idTokenResult) => {
          this.uporabnikJeAdmin = !!idTokenResult.claims.admin;
          // console.log('admin? ' + user.admin);
          this.uporabnikJeUpravljalec = !!idTokenResult.claims.upravljalecDogodkov;
          // console.log('upravljalecDogodkov? ' + user.upravljalecDogodkov);
          this.uporabnik = user;
        });
      }
    });

  }

  /**
   * @description preverimo, ali je uporabnik že potrdil svoj račun. Prošnjo za potrditev dobi kot sporočilo na svoj email naslov.
   *
   * @param email email
   */
  preveri_uporabnika() {
    const user = JSON.parse(localStorage.getItem('user'));
    return (user !== null) ? true : false;
  }

  preveri_veljavnost_emaila(email) {
    // tslint:disable-next-line: max-line-length
    const patt = new RegExp('/^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/');
    return email.test(patt);
  }

  preveri_veljavnost_gesla(geslo1, geslo2) {
    return (geslo1 === geslo2);
  }

  // spremeni_geslo(email, geslo)
  spremeni_geslo(geslo) {
    const user = this.afAuth.auth.currentUser;
    user.updatePassword(geslo).then(() => {
      // Update successful.
    }).catch(() => {
      // An error happened.
    });
  }

  // poslji_potrditveni_email(email)
  poslji_potrditveni_email() {
    return this.afAuth.auth.currentUser.sendEmailVerification()
    .then(() => {
      // this.router.navigate(['verify-email-address']);
    });
  }

  /**
   * dodaj_uporabnika (LOGIN)
   * @description podani email naslov in hashed geslo shranimo v bazo za kasnejšo prijavo.
   *
   * @param email email uporabnika
   * @param geslo geslo uporabnika
   */
  dodaj_uporabnika(email, geslo) {
    return this.afAuth.auth.createUserWithEmailAndPassword(email, geslo)
      .then((result) => {
        /* Call the SendVerificaitonMail() function when new user sign
        up and returns promise */
        this.poslji_potrditveni_email();
      }).catch((error) => {
        window.alert(error.message);
      });
  }

  // TODO
  preveri_veljavnost_potrditvenega_zetona(potrditveni_zeton) {
    return false;
  }

  // TODO
  dodaj_uporabnika_v_seznam_nepotrjenih(email, potrditveni_zeton) {
    return false;
  }

  odstrani_uporabnika_iz_seznama(email) {}

  shrani_dostopni_zeton(email, casDostopa) {}

  nazaj_poslji_dostopni_piskotek() {}

  preveri_dostopni_zeton(mail, zeton) {}

  pridobi_uporabnika() {
    return JSON.parse(localStorage.getItem('user'));
  }

}
