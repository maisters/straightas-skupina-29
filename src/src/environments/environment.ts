// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebaseConfig: {
    apiKey: "AIzaSyA2C00sAOdibgsFLaq2qphwxhmOvLzB9w8",
    authDomain: "straightas-skupina29.firebaseapp.com",
    databaseURL: "https://straightas-skupina29.firebaseio.com",
    projectId: "straightas-skupina29",
    storageBucket: "straightas-skupina29.appspot.com",
    messagingSenderId: "1019921925257",
    appId: "1:1019921925257:web:993d13908356fab6"
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
