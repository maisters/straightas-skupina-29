# Lastni projekt pri predmetu TPO

Vsaka skupina, ki je sestavljena iz 4 članov, mora razviti lastni projekt (LP) na izbrani problemski domeni, in sicer od **predloga projekta** do **implementacije**, kjer je podrobna razdelitev naslednja:

* **1. LP** - [Predlog projekta](docs/predlog-projekta),
* **2. LP** - [Zajem zahtev](docs/zajem-zahtev),
* **3. LP** - [Načrt rešitve](docs/nacrt) in
* **4. LP** - [Implementacija](src).

Za uporabo aplikacije je potrebno le skopirati vsebino direktorija src/dist/StraightAs 
na katerikoli spletni strežnik, ki lahko streže statične datoteke (Apache, Nginx, Python Flask, node.js Express, ...).


**testni uporabniki:**

- _uporabnik2@uporabnik.com_ / `uporabnik2`
- _admin@admin.com_ / `admin1`
- _upravljalec@upravljalec.com_ / `upravljalec`

**javno dostopna aplikacija je na voljo na naslovu:**
[https://straightas-skupina29.firebaseapp.com](https://straightas-skupina29.firebaseapp.com)